#!/usr/local/var/rbenv/shims/ruby

require "bundler"
Bundler.require(:default)

Dotenv.load

require "active_support/core_ext/hash/keys"

require "./services/ambient_weather"
require "./services/wunderground_forecast"
require "./services/weatherbit"
require "./utilities/logger"

KMH_TO_KT = 1.852
MPH_TO_KMH = 1.609344
MPH_TO_KT = 0.868976

INHG_TO_HPA = 33.86389

def f_to_c(temp)
  (temp - 32) / 1.8
end

def in_to_mm(mesure)
  mesure * 25.4
end

logger = Utilities::Logger.instance

logger.info "Retrieving webcam image"

rtsp_url = "rtsp://#{ENV['RTSP_URL']}"

raw_filename = "birdhouse-weathercam.jpg"
processed_filename = "birdhouse-weathercam-processed.jpg"
remote_filename = "last.jpg"
wu_processed_filename = "birdhouse-weathercam-processed-wu.jpg"
wu_remote_filename = "last_wu.jpg"

ffmpeg_cmd = "/usr/local/bin/ffmpeg -v 16 -y -rtsp_transport tcp -i #{rtsp_url} -frames:v 1 #{raw_filename}"
logger.info ffmpeg_cmd
system(ffmpeg_cmd)

logger.info "Retrieving current observations and forecast"

# observations = Services::WundergroundPwsObervations.client.current(station_id: ENV["WUNDERGROUND_STATION_ID"])[:observations][0]
# observations_e = Services::WundergroundPwsObervations.client.current(station_id: ENV["WUNDERGROUND_STATION_ID"], units: "e")[:observations][0]

observations = Services::AmbientWeather.client.devices(mac_address: ENV["AMBIENT_WEATHER_MAC_ADDRESS"], limit: 1)

# forecast = Services::WundergroundForecast.client.five_day(place_id: ENV["WUNDERGROUND_PLACE_ID"])

# aqi = Services::WeatherbitAQI.client.airquality([37.87370, -122.28198])

forecast = Services::Weatherbit.client.current([37.87370, -122.28198]).dig(:data, 0)

logger.info "Processing data"

w = {
  narrative: forecast[:weather][:description],
  icon_code: forecast[:weather][:icon],
  uv: observations[:uv],
  solarradiation: observations[:solarradiation],
  humidity: observations[:humidity],
  windir: observations[:winddir],
  windir_name: Services::Weatherbit::WINDIR_NAMES.select { |range| range.include?(observations[:winddir]) }.values.first,
  pressure: (observations[:baromrelin] * INHG_TO_HPA).round(1),
  temp_m: f_to_c(observations[:tempf]).round(1),
  temp_e: observations[:tempf],
  precip_event_m: in_to_mm(observations[:eventrainin]).round(1),
  precip_event_e: observations[:eventrainin],
  dewpt_m: f_to_c(observations[:dewPoint]).round(2),
  dewpt_e: observations[:dewPoint],
  wind_speed_m: (observations[:windspeedmph] * MPH_TO_KMH).round,
  wind_speed_kt: (observations[:windspeedmph] * MPH_TO_KT).round,
  wind_gust_m: (observations[:windgustmph] * MPH_TO_KMH).round,
  wind_gust_kt: (observations[:windgustmph] * MPH_TO_KT).round,
  aqi: forecast[:aqi],
  aqi_level: Services::Weatherbit::AQI_LEVELS.select { |range| range.include?(forecast[:aqi]) }.values.first[:level]
}

playing_now = nil

logger.info "Composing image"

margin = 20

cam_im = Vips::Image.new_from_file(raw_filename)
weather_icon = Vips::Image.new_from_file("wb-icons/#{w[:icon_code]}.png")
logo = Vips::Image.new_from_file("logo.png")

new_im = cam_im.composite(weather_icon, :over, x: margin, y: cam_im.height - weather_icon.height - margin)
new_im = new_im.composite(logo, :over, x: cam_im.width - logo.width - margin, y: margin)

# TOP TEXT

top_text_content = [
  "#{Time.now.strftime('%A %B %-d, %Y %T %Z')} – birdhousecollective.org", # E.g.: Monday Feburary 8, 2021 13:23:10 PST
  ("🎶 Playing now on KBHC: #{playing_now}" if playing_now)
].compact.join("\n")

top_text_x = margin
top_text_y = margin

top_text_shadow = Vips::Image.text top_text_content, width: cam_im.width - margin * 2, dpi: 150, font: "sans"
shadow_color = (top_text_shadow.new_from_image [0, 0, 0]).copy(interpretation: :srgb)
top_text_shadow = shadow_color.bandjoin(top_text_shadow)
new_im = new_im.composite(top_text_shadow, :over, x: top_text_x - 1, y: top_text_y - 1)
new_im = new_im.composite(top_text_shadow, :over, x: top_text_x + 1, y: top_text_y + 1)
new_im = new_im.composite(top_text_shadow, :over, x: top_text_x - 1, y: top_text_y + 1)
new_im = new_im.composite(top_text_shadow, :over, x: top_text_x + 1, y: top_text_y - 1)

top_text = Vips::Image.text top_text_content, width: cam_im.width - margin * 2, dpi: 150, font: "sans"
color = (top_text.new_from_image [255, 255, 255]).copy(interpretation: :srgb)
colored_top_text = color.bandjoin(top_text)
new_im = new_im.composite(colored_top_text, :over, x: top_text_x, y: top_text_y)

# BOTTOM TEXT

bottom_text_x = margin + weather_icon.width + margin
bottom_text_y = cam_im.height - 200 - margin

bottom_text_content = [
  "#{w[:narrative]}",
  "Temperature: #{w[:temp_m]}°C | #{w[:temp_e]}°F    Dew point: #{w[:dewpt_m]}°C | #{w[:dewpt_e]}°F    UV: #{w[:uv]} (#{w[:solarradiation]}W/m²)",
  "Humidity: #{w[:humidity]}%    Precipitation: #{w[:precip_event_m]}mm | #{w[:precip_event_e]}in    Pressure: #{w[:pressure]}hPa",
  "Wind: #{w[:windir]}° #{w[:windir_name]} #{w[:wind_speed_m]}km/h | #{w[:wind_speed_kt]}kt    Gust: #{w[:wind_gust_m]}km/h | #{w[:wind_gust_kt]}kt    AQI: #{w[:aqi]} (#{w[:aqi_level]})",
].join("\n")

text_shadow = Vips::Image.text bottom_text_content, width: cam_im.width - margin * 2 - 200, height: 200, font: "sans"
shadow_color = (text_shadow.new_from_image [0, 0, 0]).copy(interpretation: :srgb)
text_shadow = shadow_color.bandjoin(text_shadow)
new_im = new_im.composite(text_shadow, :over, x: bottom_text_x - 1, y: bottom_text_y - 1)
new_im = new_im.composite(text_shadow, :over, x: bottom_text_x + 1, y: bottom_text_y + 1)
new_im = new_im.composite(text_shadow, :over, x: bottom_text_x - 1, y: bottom_text_y + 1)
new_im = new_im.composite(text_shadow, :over, x: bottom_text_x + 2, y: bottom_text_y - 1)

text = Vips::Image.text bottom_text_content, width: cam_im.width - margin * 2 - 200, height: 200, font: "sans"
overlay = (text.new_from_image [255, 255, 255]).copy(interpretation: :srgb)
overlay = overlay.bandjoin(text)
new_im = new_im.composite(overlay, :over, x: bottom_text_x, y: bottom_text_y)

new_im.write_to_file(processed_filename)

wu_im = new_im.resize(0.666667)
wu_im.write_to_file(wu_processed_filename)

system("open", processed_filename) if ENV["OPEN_PROCESSED_FILE"] == "true"

logger.info "Sending images to server"

scp_cmd = "scp -q -i #{ENV['SCP_KEY_PATH']} #{processed_filename} scp://#{ENV['SCP_SERVER']}/#{remote_filename}"
logger.info scp_cmd
system(scp_cmd)

scp_cmd = "scp -q -i #{ENV['SCP_KEY_PATH']} #{wu_processed_filename} scp://#{ENV['SCP_SERVER']}/#{wu_remote_filename}"
logger.info scp_cmd
system(scp_cmd)
