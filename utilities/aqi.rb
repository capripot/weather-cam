
module Utilities
  # Air Quality Indicator calculator
  #
  # PM2.5 particules < 2.5µm
  # PM10 particules < 10µm
  # O3: Ozone
  # NO2: Nitrogen Dioxyde
  # CO: Carbone monxyde
  # SO2: Sulfure Dioxyde
  #
  # Original paper: https://www.airnow.gov/sites/default/files/2018-06/air-quality-index-reporting-final-rule.pdf
  # Breakpoints: https://aqs.epa.gov/aqsweb/documents/codetables/aqi_breakpoints.html
  # Equation: https://forum.airnowtech.org/t/the-aqi-equation/169
  #
  class AQI
    def calculate(type:, values:)
      available_types = %i[pm25 pm10 o3 no2 co so2]
      raise "Type must be one of #{available_types.join(', ')}" unless available_types.include?(type)

      avg = (values.sum(0.0) / values.size).truncate(3)
      breakpoint = call("#{type}_breakpoints").select { |key| key.include?(avg) }
      conc_lo_hi = breakpoint.keys.first
      aqi_lo_hi = breakpoint.values.first

      (aqi_lo_hi[1] - aqi_lo_hi[0]) / (conc_lo_hi[1] - conc_lo_hi[0]) * (avg - conc_lo_hi[0]) + aqi_lo_hi[0]
    end

    def pm25_breakpoints
      {
        0.0..12.0 => [0, 50],
        12.1..35.4 => [51, 100],
        35.5..55.4 => [101, 150],
        55.5..150.4 => [151, 200],
        150.5..250.4 => [201, 300],
        250.5..350.4 => [301, 400],
        350.5..500.4 => [401, 500],
        500.5..999_999.9 => [501, 999]
      }
    end

    def pm10_breakpoints
      {
        0.0..54.0 => [0, 50],
        55.0..154.0 => [51, 100],
        155.0..254.0 => [101, 150],
        255.0..354.0 => [151, 200],
        355.0..424.0 => [201, 300],
        425.0..504.0 => [301, 400],
        505.0..604.0 => [401, 500],
        605.0..999_999.0 => [501, 999]
      }
    end

    def o3_breakpoints
      {
        0.0..0.124 => [-1, -1],
        0.125..0.164 => [101, 150],
        0.165..0.204 => [151, 200],
        0.205..0.404 => [201, 300],
        0.405..0.504 => [301, 400],
        0.505..0.604 => [401, 500],
        0.605..99_999.0 => [501, 999]
      }
    end
  end
end
