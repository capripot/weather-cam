require 'open-uri'

(0..47).each do |i|
  filename = "#{'%02d' % i}.svg"
  open("wu-icons/#{filename}", "wb") do |file|
    URI.open("https://www.wunderground.com/static/i/c/v4/#{filename}") do |remote|
      file.write(remote.read)
      sleep 0.1
    end
  end
end
