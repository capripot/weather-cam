require "logger"
require "singleton"
require "active_support/core_ext/module/delegation"

module Utilities
  # Singleton logger
  class Logger
    include Singleton

    delegate :debug, :info, :warn, :error, :fatal, to: :@logger

    def initialize
      @logger = ::Logger.new("logs.log", 4, 1_024_000)
    end
  end
end
