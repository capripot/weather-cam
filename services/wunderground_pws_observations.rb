require "httparty"

require_relative "../utilities/logger"

module Services
  # Weather Underground Personal Weather Station Current Observations API
  # https://docs.google.com/document/d/1eKCnKXI9xnoMGRRzOL1xPCBihNV2rOet08qpE_gArAY/edit
  # https://docs.google.com/document/d/1KGb8bTVYRsNgljnNH67AMhckY8AQT2FVwZ9urj8SWBs/edit
  class WundergroundPwsObervations
    include HTTParty

    base_uri "https://api.weather.com/v2/pws/observations"
    headers "Content-Type" => "application/json"

    WINDIR_NAMES = {
      0..11.25 => "N",
      11.26..33.75 => "NNE",
      33.76..56.25 => "NE",
      56.26..78.25 => "ENE",
      78.26..101.25 => "E",
      101.26..123.75 => "ESE",
      123.76..146.25 => "SE",
      146.26..168.75 => "SSE",
      168.76..191.25 => "S",
      191.26..213.75 => "SSW",
      213.76..236.25 => "SW",
      236.26..258.75 => "WSW",
      258.76..281.25 => "W",
      281.26..303.75 => "WNW",
      303.76..326.25 => "NW",
      326.26..348.75 => "NNW",
      348.76..360 => "N"
    }.freeze

    def self.client
      @client ||= new
    end

    def current(station_id:, units: "m")
      raise "Unit must be m or e" unless %w[e m].include?(units)

      rep = self.class.get("/current",
                           query: {
                             stationId: station_id,
                             format: "json",
                             units: units,
                             apiKey: ENV["WUNDERGROUND_API_KEY"]
                           })
      unless rep.success?
        Utilities::Logger.instance.error("#{rep.code} #{rep}")
        return {}
      end

      rep.deep_symbolize_keys
    rescue StandardError => e
      Utilities::Logger.instance.error(e.message)
    end
  end
end

### Example of responses

#   Imperial
#
#     {
#       "observations": [
#           {
#               "stationID": "KCABERKE221",
#               "obsTimeUtc": "2021-02-05T08:42:49Z",
#               "obsTimeLocal": "2021-02-05 00:42:49",
#               "neighborhood": "Central",
#               "softwareType": "AMBWeatherV4.2.8",
#               "country": "US",
#               "solarRadiation": 0.0,
#               "lon": -122.282,
#               "realtimeFrequency": nil,
#               "epoch": 1612514569,
#               "lat": 37.874,
#               "uv": 0.0,
#               "winddir": 259,
#               "humidity": 71,
#               "qcStatus": 1,
#               "imperial": {
#                   "temp": 47,
#                   "heatIndex": 47,
#                   "dewpt": 38,
#                   "windChill": 47,
#                   "windSpeed": 1,
#                   "windGust": 2,
#                   "pressure": 30.28,
#                   "precipRate": 0.00,
#                   "precipTotal": 0.00,
#                   "elev": 134
#               }
#           }
#       ]
#     }
#
#
#   Metric
#
#     {
#       "observations": [
#           {
#               "stationID": "KCABERKE221",
#               "obsTimeUtc": "2021-02-05T08:43:53Z",
#               "obsTimeLocal": "2021-02-05 00:43:53",
#               "neighborhood": "Central",
#               "softwareType": "AMBWeatherV4.2.8",
#               "country": "US",
#               "solarRadiation": 0.0,
#               "lon": -122.282,
#               "realtimeFrequency": nil,
#               "epoch": 1612514633,
#               "lat": 37.874,
#               "uv": 0.0,
#               "winddir": 259,
#               "humidity": 71,
#               "qcStatus": 1,
#               "metric": {
#                   "temp": 8,
#                   "heatIndex": 8,
#                   "dewpt": 3,
#                   "windChill": 8,
#                   "windSpeed": 0,
#                   "windGust": 2,
#                   "pressure": 1025.03,
#                   "precipRate": 0.00,
#                   "precipTotal": 0.00,
#                   "elev": 41
#               }
#           }
#       ]
#     }

