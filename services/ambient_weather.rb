module Services
  # https://ambientweather.docs.apiary.io/#
  # https://github.com/ambient-weather/api-docs/wiki/Device-Data-Specs
  class AmbientWeather
    include HTTParty

    base_uri "https://api.ambientweather.net/v1"
    headers "Content-Type" => "application/json",
            "Accept" => "application/json"

    def self.client
      @client ||= new
    end

    def devices(mac_address:, end_date: nil, limit: 288)
      query = {
        apiKey: ENV["AMBIENT_WEATHER_API_KEY"],
        applicationKey: ENV["AMBIENT_WEATHER_APPLICATION_KEY"],
        limit: limit
      }
      query.merge!(end_date: end_date) unless end_date.nil?

      resp = self.class.get("/devices/#{mac_address}", query: query)

      unless resp.success?
        Utilities::Logger.instance.error("#{resp.code} #{resp}")
        return {}
      end

      resp.first.deep_symbolize_keys
    rescue StandardError => e
      Utilities::Logger.instance.error(e.message)
    end
  end
end
