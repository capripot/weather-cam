require "httparty"

module Services
  # Purple Air API
  # https://api.purpleair.com/#api-sensors-get-sensors-data
  class PurpleAir
    include HTTParty

    base_uri "https://api.purpleair.com/v1"
    headers "accept" => "application/json; charset=utf-8",
            "x-api-key" => ENV["PURPLE_AIR_API_KEY"],
            "user-agent" => "Haze-iOS/2 CFNetwork/1220.1 Darwin/20.3.0",
            "accept-language" => "en-us",
            "accept-encoding" => "gzip, deflate, br"

    def self.client
      @client ||= new
    end

    # Bounding box format: { nw: [lat, lng], se: [lat, lng] }
    def sensors(bounding_box: nil)
      rep = self.class.get("/sensors",
                           query: {
                             fields: %w[name latitude longitude last_seen channel_state
                                        channel_flags confidence pm1.0 pm2.5_24hour pm10.0
                                        ozone1].join(","),
                             location_type: 0,
                             modified_since: Time.now.to_i,
                             max_age: 86_400,
                             nwlat: bounding_box[:nw][0],
                             nwlng: bounding_box[:nw][1],
                             selat: bounding_box[:se][0],
                             selng: bounding_box[:se][1]
                           })
      rep.deep_symbolize_keys
    end
  end
end


### Response example


# {
#     "api_version": "V1.0.6-0.0.9",
#     "time_stamp": 1612723290,
#     "data_time_stamp": 1612723278,
#     "location_type": 0,
#     "modified_since": 1612720745,
#     "max_age": 86400,
#     "fields": [
#         "sensor_index",
#         "name",
#         "latitude",
#         "longitude",
#         "last_seen",
#         "channel_state",
#         "channel_flags",
#         "confidence",
#         "pm1.0",
#         "pm2.5",
#         "pm10.0"
#     ],
#     "channel_states": [
#         "No PM",
#         "PM-A",
#         "PM-B",
#         "PM-A+PM-B"
#     ],
#     "channel_flags": [
#         "Normal",
#         "A-Downgraded",
#         "B-Downgraded",
#         "A+B-Downgraded"
#     ],
#     "data": [
#         [
#             20,
#             "Oakdale",
#             40.6031,
#             -111.8361,
#             1612723181,
#             1,
#             2,
#             30,
#             0.0,
#             0.0,
#             0.0
#         ],
#         [
#             47,
#             "OZONE TEST",
#             40.4762,
#             -111.8826,
#             1612723133,
#             0,
#             3,
#             30,
#             null,
#             null,
#             null
#         ],
#         [
#             53,
#             "Lakeshore",
#             40.2467,
#             -111.7048,
#             1612723261,
#             1,
#             0,
#             30,
#             0.0,
#             0.0,
#             0.0
#         ],
#         [
#             72,
#             "90.9fm KRCL",
#             40.7702,
#             -111.9471,
#             1612723267,
#             1,
#             0,
#             30,
#             1.2,
#             1.3,
#             1.6
#         ],
#         [
#             74,
#             "Wasatch Commons",
#             40.7383,
#             -111.9362,
#             1612722230,
#             1,
#             0,
#             30,
#             0.0,
#             0.0,
#             0.0
#         ]
#     ]
# }
