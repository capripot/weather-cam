require "httparty"

require_relative "../utilities/logger"

module Services
  # Weatherbit API
  # https://www.weatherbit.io/api/weather-current
  # https://www.weatherbit.io/api/airquality-current
  class Weatherbit
    include HTTParty

    base_uri "https://api.weatherbit.io/v2.0/current"
    headers "accept" => "application/json; charset=utf-8",
            "accept-language" => "en-us"

    AQI_LEVELS = {
      0..50 => { level: "Good", color: :green, narrative: "Air quality is satisfactory, and air pollution poses little or no risk." },
      51..100 => { level: "Moderate", color: :yellow, narrative: "Air quality is acceptable. However, there may be a risk for some people, particularly those who are unusually sensitive to air pollution." },
      101..150 => { level: "Unhealthy for Sensitive Groups", color: :orange, narrative: "Members of sensitive groups may experience health effects. The general public is less likely to be affected." },
      151..200 => { level: "Unhealthy", color: :red, narrative: "Some members of the general public may experience health effects; members of sensitive groups may experience more serious health effects." },
      201..300 => { level: "Very Unhealthy", color: :purple, narrative: "Health alert: The risk of health effects is increased for everyone." },
      301..999 => { level: "Hazardous", color: :maroon, narrative: "Health warning of emergency conditions: everyone is more likely to be affected." },
    }.freeze

    WINDIR_NAMES = {
      0..11.25 => "N",
      11.26..33.75 => "NNE",
      33.76..56.25 => "NE",
      56.26..78.25 => "ENE",
      78.26..101.25 => "E",
      101.26..123.75 => "ESE",
      123.76..146.25 => "SE",
      146.26..168.75 => "SSE",
      168.76..191.25 => "S",
      191.26..213.75 => "SSW",
      213.76..236.25 => "SW",
      236.26..258.75 => "WSW",
      258.76..281.25 => "W",
      281.26..303.75 => "WNW",
      303.76..326.25 => "NW",
      326.26..348.75 => "NNW",
      348.76..360 => "N"
    }.freeze

    def self.client
      @client ||= new
    end

    def current(lat_lng)
      resp = self.class.get("/",
                            query: {
                              lat: lat_lng[0],
                              lon: lat_lng[1],
                              key: ENV["WEATHERBIT_API_KEY"]
                            })

      unless resp.success?
        Utilities::Logger.instance.error("#{resp.code} #{resp}")
        return {}
      end

      resp.deep_symbolize_keys
    rescue StandardError => e
      Utilities::Logger.instance.error(e.message)
    end

    def airquality(lat_lng)
      resp = self.class.get("/airquality",
                            query: {
                              lat: lat_lng[0],
                              lon: lat_lng[1],
                              key: ENV["WEATHERBIT_API_KEY"]
                            })

      unless resp.success?
        Utilities::Logger.instance.error("#{resp.code} #{resp}")
        return {}
      end

      resp.deep_symbolize_keys
    rescue StandardError => e
      Utilities::Logger.instance.error(e.message)
    end
  end
end
