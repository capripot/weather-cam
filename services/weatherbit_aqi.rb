require "httparty"

require_relative "../utilities/logger"

module Services
  # Weatherbit Air Quality API
  # https://www.weatherbit.io/api/airquality-current
  class WeatherbitAQI
    include HTTParty

    base_uri "https://api.weatherbit.io/v2.0/current"
    headers "accept" => "application/json; charset=utf-8",
            "accept-language" => "en-us"

    LEVELS = {
      0..50 => { level: "Good", color: :green, narrative: "Air quality is satisfactory, and air pollution poses little or no risk." },
      51..100 => { level: "Moderate", color: :yellow, narrative: "Air quality is acceptable. However, there may be a risk for some people, particularly those who are unusually sensitive to air pollution." },
      101..150 => { level: "Unhealthy for Sensitive Groups", color: :orange, narrative: "Members of sensitive groups may experience health effects. The general public is less likely to be affected." },
      151..200 => { level: "Unhealthy", color: :red, narrative: "Some members of the general public may experience health effects; members of sensitive groups may experience more serious health effects." },
      201..300 => { level: "Very Unhealthy", color: :purple, narrative: "Health alert: The risk of health effects is increased for everyone." },
      301..999 => { level: "Hazardous", color: :maroon, narrative: "Health warning of emergency conditions: everyone is more likely to be affected." },
    }.freeze

    def self.client
      @client ||= new
    end

    # Bounding box format: { nw: [lat, lng], se: [lat, lng] }
    def airquality(lat_lng)
      resp = self.class.get("/airquality",
                           query: {
                             lat: lat_lng[0],
                             lon: lat_lng[1],
                             key: ENV["WEATHERBIT_API_KEY"]
                           })

      Utilities::Logger.instance.error("#{resp.code} #{resp}") unless resp.success?

      resp.deep_symbolize_keys
    rescue StandardError => e
      Utilities::Logger.instance.error(e.message)
    end
  end
end
