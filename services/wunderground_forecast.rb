require "httparty"

require_relative "../utilities/logger"

module Services
  # Weather Underground 5 Days forecast API
  # https://docs.google.com/document/d/1eKCnKXI9xnoMGRRzOL1xPCBihNV2rOet08qpE_gArAY/edit
  # https://docs.google.com/document/d/1_Zte7-SdOjnzBttb1-Y9e0Wgl0_3tah9dSwXUyEA3-c/edit
  class WundergroundForecast
    include HTTParty

    base_uri "https://api.weather.com/v3/wx/forecast/daily"
    headers "Content-Type" => "application/json"

    def self.client
      @client ||= new
    end

    def five_day(place_id:, units: "m", language: "en-US")
      raise "Unit must be m or e" unless %w[e m].include?(units)

      resp = self.class.get("/5day",
                            query: {
                              placeid: place_id,
                              format: "json",
                              language: language,
                              units: units,
                              apiKey: ENV["WUNDERGROUND_API_KEY"]
                            })
      unless resp.success?
        Utilities::Logger.instance.error("#{resp.code} #{resp}")
        return {}
      end

      resp.deep_symbolize_keys
    rescue StandardError => e
      Utilities::Logger.instance.error(e.message)
    end
  end
end

### Example of response

# {
#     "dayOfWeek": [
#         "Thursday",
#         "Friday",
#         "Saturday",
#         "Sunday",
#         "Monday",
#         "Tuesday"
#     ],
#     "expirationTimeUtc": [
#         1612505408,
#         1612505408,
#         1612505408,
#         1612505408,
#         1612505408,
#         1612505408
#     ],
#     "moonPhase": [
#         "Last Quarter",
#         "Waning Crescent",
#         "Waning Crescent",
#         "Waning Crescent",
#         "Waning Crescent",
#         "Waning Crescent"
#     ],
#     "moonPhaseCode": [
#         "LQ",
#         "WNC",
#         "WNC",
#         "WNC",
#         "WNC",
#         "WNC"
#     ],
#     "moonPhaseDay": [
#         22,
#         23,
#         24,
#         25,
#         26,
#         27
#     ],
#     "moonriseTimeLocal": [
#         "2021-02-04T00:26:05-0800",
#         "2021-02-05T01:37:20-0800",
#         "2021-02-06T02:47:45-0800",
#         "2021-02-07T03:56:37-0800",
#         "2021-02-08T05:00:12-0800",
#         "2021-02-09T05:56:24-0800"
#     ],
#     "moonriseTimeUtc": [
#         1612427165,
#         1612517840,
#         1612608465,
#         1612698997,
#         1612789212,
#         1612878984
#     ],
#     "moonsetTimeLocal": [
#         "2021-02-04T11:23:35-0800",
#         "2021-02-05T12:00:24-0800",
#         "2021-02-06T12:43:21-0800",
#         "2021-02-07T13:34:11-0800",
#         "2021-02-08T14:31:32-0800",
#         "2021-02-09T15:35:29-0800"
#     ],
#     "moonsetTimeUtc": [
#         1612466615,
#         1612555224,
#         1612644201,
#         1612733651,
#         1612823492,
#         1612913729
#     ],
#     "narrative": [
#         "Partly cloudy. Low 5C.",
#         "Mainly sunny. Highs 16 to 18C and lows 5 to 7C.",
#         "Sunny. Highs 16 to 18C and lows 5 to 7C.",
#         "Plenty of sun. Highs 16 to 18C and lows 6 to 8C.",
#         "More clouds than sun. Highs 16 to 18C and lows 7 to 9C.",
#         "Morning clouds followed by afternoon sun. Highs 14 to 16C and lows 7 to 9C."
#     ],
#     "qpf": [
#         0.0,
#         0.0,
#         0.0,
#         0.0,
#         0.0,
#         0.0
#     ],
#     "qpfSnow": [
#         0.0,
#         0.0,
#         0.0,
#         0.0,
#         0.0,
#         0.0
#     ],
#     "sunriseTimeLocal": [
#         "2021-02-04T07:10:25-0800",
#         "2021-02-05T07:09:27-0800",
#         "2021-02-06T07:08:27-0800",
#         "2021-02-07T07:07:26-0800",
#         "2021-02-08T07:06:23-0800",
#         "2021-02-09T07:05:20-0800"
#     ],
#     "sunriseTimeUtc": [
#         1612451425,
#         1612537767,
#         1612624107,
#         1612710446,
#         1612796783,
#         1612883120
#     ],
#     "sunsetTimeLocal": [
#         "2021-02-04T17:36:45-0800",
#         "2021-02-05T17:37:52-0800",
#         "2021-02-06T17:38:59-0800",
#         "2021-02-07T17:40:05-0800",
#         "2021-02-08T17:41:11-0800",
#         "2021-02-09T17:42:17-0800"
#     ],
#     "sunsetTimeUtc": [
#         1612489005,
#         1612575472,
#         1612661939,
#         1612748405,
#         1612834871,
#         1612921337
#     ],
#     "temperatureMax": [
#         null,
#         17,
#         17,
#         17,
#         17,
#         15
#     ],
#     "temperatureMin": [
#         5,
#         6,
#         6,
#         7,
#         8,
#         8
#     ],
#     "validTimeLocal": [
#         "2021-02-04T07:00:00-0800",
#         "2021-02-05T07:00:00-0800",
#         "2021-02-06T07:00:00-0800",
#         "2021-02-07T07:00:00-0800",
#         "2021-02-08T07:00:00-0800",
#         "2021-02-09T07:00:00-0800"
#     ],
#     "validTimeUtc": [
#         1612450800,
#         1612537200,
#         1612623600,
#         1612710000,
#         1612796400,
#         1612882800
#     ],
#     "daypart": [
#         {
#             "cloudCover": [
#                 null,
#                 37,
#                 8,
#                 35,
#                 12,
#                 45,
#                 32,
#                 80,
#                 75,
#                 79,
#                 69,
#                 64
#             ],
#             "dayOrNight": [
#                 null,
#                 "N",
#                 "D",
#                 "N",
#                 "D",
#                 "N",
#                 "D",
#                 "N",
#                 "D",
#                 "N",
#                 "D",
#                 "N"
#             ],
#             "daypartName": [
#                 null,
#                 "Tonight",
#                 "Tomorrow",
#                 "Tomorrow night",
#                 "Saturday",
#                 "Saturday night",
#                 "Sunday",
#                 "Sunday night",
#                 "Monday",
#                 "Monday night",
#                 "Tuesday",
#                 "Tuesday night"
#             ],
#             "iconCode": [
#                 null,
#                 29,
#                 32,
#                 29,
#                 32,
#                 29,
#                 34,
#                 27,
#                 28,
#                 27,
#                 30,
#                 27
#             ],
#             "iconCodeExtend": [
#                 null,
#                 2900,
#                 3200,
#                 2900,
#                 3200,
#                 2900,
#                 3400,
#                 2700,
#                 2800,
#                 2700,
#                 9003,
#                 2700
#             ],
#             "narrative": [
#                 null,
#                 "Clear early then increasing cloudiness after midnight. Low around 5C. Winds light and variable.",
#                 "Sunny. High 17C. Winds light and variable.",
#                 "Clear evening skies will give way to mostly cloudy skies overnight. Low 6C. Winds light and variable.",
#                 "Mainly sunny. High 17C. Winds light and variable.",
#                 "Mostly clear early then increasing cloudiness overnight. Low 6C. Winds light and variable.",
#                 "Sunny along with a few clouds. High 17C. Winds NW at 10 to 15 km/h.",
#                 "Partly cloudy skies during the evening will give way to cloudy skies overnight. Low 7C. Winds light and variable.",
#                 "Mostly cloudy skies. High 17C. Winds SW at 10 to 15 km/h.",
#                 "Mostly cloudy. Low 8C. Winds light and variable.",
#                 "Cloudy skies early will become partly cloudy later in the day. High around 15C. Winds SW at 10 to 15 km/h.",
#                 "Partly cloudy skies in the evening, then becoming cloudy overnight. Low 8C. Winds light and variable."
#             ],
#             "precipChance": [
#                 null,
#                 5,
#                 4,
#                 8,
#                 6,
#                 10,
#                 7,
#                 8,
#                 8,
#                 15,
#                 14,
#                 24
#             ],
#             "precipType": [
#                 null,
#                 "rain",
#                 "rain",
#                 "rain",
#                 "rain",
#                 "rain",
#                 "rain",
#                 "rain",
#                 "rain",
#                 "rain",
#                 "rain",
#                 "rain"
#             ],
#             "qpf": [
#                 null,
#                 0.0,
#                 0.0,
#                 0.0,
#                 0.0,
#                 0.0,
#                 0.0,
#                 0.0,
#                 0.0,
#                 0.0,
#                 0.0,
#                 0.0
#             ],
#             "qpfSnow": [
#                 null,
#                 0.0,
#                 0.0,
#                 0.0,
#                 0.0,
#                 0.0,
#                 0.0,
#                 0.0,
#                 0.0,
#                 0.0,
#                 0.0,
#                 0.0
#             ],
#             "qualifierCode": [
#                 null,
#                 null,
#                 null,
#                 null,
#                 null,
#                 null,
#                 null,
#                 null,
#                 null,
#                 null,
#                 null,
#                 null
#             ],
#             "qualifierPhrase": [
#                 null,
#                 null,
#                 null,
#                 null,
#                 null,
#                 null,
#                 null,
#                 null,
#                 null,
#                 null,
#                 null,
#                 null
#             ],
#             "relativeHumidity": [
#                 null,
#                 78,
#                 61,
#                 79,
#                 63,
#                 83,
#                 68,
#                 85,
#                 69,
#                 83,
#                 71,
#                 84
#             ],
#             "snowRange": [
#                 null,
#                 "",
#                 "",
#                 "",
#                 "",
#                 "",
#                 "",
#                 "",
#                 "",
#                 "",
#                 "",
#                 ""
#             ],
#             "temperature": [
#                 null,
#                 5,
#                 17,
#                 6,
#                 17,
#                 6,
#                 17,
#                 7,
#                 17,
#                 8,
#                 15,
#                 8
#             ],
#             "temperatureHeatIndex": [
#                 null,
#                 8,
#                 16,
#                 11,
#                 16,
#                 12,
#                 16,
#                 12,
#                 16,
#                 12,
#                 15,
#                 12
#             ],
#             "temperatureWindChill": [
#                 null,
#                 6,
#                 6,
#                 7,
#                 6,
#                 7,
#                 7,
#                 7,
#                 7,
#                 8,
#                 8,
#                 8
#             ],
#             "thunderCategory": [
#                 null,
#                 "No thunder",
#                 "No thunder",
#                 "No thunder",
#                 "No thunder",
#                 "No thunder",
#                 "No thunder",
#                 "No thunder",
#                 "No thunder",
#                 "No thunder",
#                 "No thunder",
#                 "No thunder"
#             ],
#             "thunderIndex": [
#                 null,
#                 0,
#                 0,
#                 0,
#                 0,
#                 0,
#                 0,
#                 0,
#                 0,
#                 0,
#                 0,
#                 0
#             ],
#             "uvDescription": [
#                 null,
#                 "Low",
#                 "Moderate",
#                 "Low",
#                 "Moderate",
#                 "Low",
#                 "Moderate",
#                 "Low",
#                 "Low",
#                 "Low",
#                 "Moderate",
#                 "Low"
#             ],
#             "uvIndex": [
#                 null,
#                 0,
#                 3,
#                 0,
#                 3,
#                 0,
#                 3,
#                 0,
#                 2,
#                 0,
#                 3,
#                 0
#             ],
#             "windDirection": [
#                 null,
#                 114,
#                 323,
#                 63,
#                 336,
#                 132,
#                 319,
#                 175,
#                 231,
#                 212,
#                 220,
#                 207
#             ],
#             "windDirectionCardinal": [
#                 null,
#                 "ESE",
#                 "NW",
#                 "ENE",
#                 "NNW",
#                 "SE",
#                 "NW",
#                 "S",
#                 "SW",
#                 "SSW",
#                 "SW",
#                 "SSW"
#             ],
#             "windPhrase": [
#                 null,
#                 "Winds light and variable.",
#                 "Winds light and variable.",
#                 "Winds light and variable.",
#                 "Winds light and variable.",
#                 "Winds light and variable.",
#                 "Winds NW at 10 to 15 km/h.",
#                 "Winds light and variable.",
#                 "Winds SW at 10 to 15 km/h.",
#                 "Winds light and variable.",
#                 "Winds SW at 10 to 15 km/h.",
#                 "Winds light and variable."
#             ],
#             "windSpeed": [
#                 null,
#                 5,
#                 9,
#                 5,
#                 9,
#                 5,
#                 12,
#                 9,
#                 12,
#                 9,
#                 12,
#                 7
#             ],
#             "wxPhraseLong": [
#                 null,
#                 "Partly Cloudy",
#                 "Sunny",
#                 "Partly Cloudy",
#                 "Sunny",
#                 "Partly Cloudy",
#                 "Mostly Sunny",
#                 "Mostly Cloudy",
#                 "Mostly Cloudy",
#                 "Mostly Cloudy",
#                 "AM Clouds/PM Sun",
#                 "Mostly Cloudy"
#             ],
#             "wxPhraseShort": [
#                 null,
#                 "P Cloudy",
#                 "Sunny",
#                 "P Cloudy",
#                 "Sunny",
#                 "P Cloudy",
#                 "M Sunny",
#                 "M Cloudy",
#                 "M Cloudy",
#                 "M Cloudy",
#                 "AM Clouds",
#                 "M Cloudy"
#             ]
#         }
#     ]
# }
