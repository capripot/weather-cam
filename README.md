# Weather Cam

![Weather cam example](weathercam-example.jpg)

## Setup

- `cp .env-example .env`
- Fill values in `.env`

### Install Dependencies

- `brew bundle`
- `rbenv install`
- `gem install bundler`
- `bundle install`

## Automate the setup

Use `launchd` to update the webcam content periodically.

- `sudo cp weather-cam.daemon.plist /Library/LaunchDaemons/`
- Replace `/path/to/weather-cam` with the path where your project is `pwd`
  - `sudo vim /Library/LaunchDaemons/weather-cam.daemon.plist`
  - `:%s#/path/to/weather-cam#PUT_PATH_HERE#g`
  - `sudo chown root:wheel /Library/LaunchDaemons/weather-cam.daemon.plist`
- `sudo launchctl load /Library/LaunchDaemons/weather-cam.daemon.plist`

To stop: `launchctl unload ~/Library/LaunchDaemons/weather-cam.daemon.plist`

To follow execution:

- `tail -f /private/var/log/system.log | grep weather` (for errors of `launchd`)
- `tail -f ./logs.log`

*Note: Don't put this program in either `~/Documents`, `~/Downloads`, or `~/Desktop`.
These folders are sandboxed and Bash don't have access to them unless you grant it full disk access.
`~/Applications` can be a good place for instance.*

## Disclaimers

### APIs used

I started by using Weather Underground. But even doing a pull every minute, I started to get 401s,
it seems it was too much to their taste.
So I gave up, and now rely on [Ambient](https://ambientweather.docs.apiary.io)
(manufacturer of my weather station) to retrieve the observations,
and [WeatherBit](https://www.weatherbit.io/api) for the current condition and AQI.
These two APIs are actually much cleaner, the only down side is lack of metric reading
from Ambient API.

### License

WeatherCam Copyright (C) 2021 Ronan Potage

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
