#!/usr/local/var/rbenv/shims/ruby

require 'rubygems'
require 'bundler/setup'
Bundler.require(:default)

class ServerResponseError < StandardError; end

class Wyze
  include HTTParty
  base_uri ENV.fetch('BASE_URL', "https://api.wyzecam.com:8443")
  headers 'Accept' => 'application/json',
          'Content-Type' => 'application/json'
  query_string_normalizer ->(query){query.to_json}

  def self.client
    @client ||= new
  end

  def initialize
    super

    @username = ENV.fetch('WYZE_USERNAME')
    @password = ENV.fetch('WYZE_PASSWORD')
    @phone_id = ENV['PHONE_ID'] || 'bc151f39-787b-4871-be27-5a20fd0a1937'
    @app_ver = ENV['APP_VER'] || 'com.hualai.WyzeCam___2.16.46'
    @sc = '9f275790cab94a72bd206c8876429f3c'
    @sv = '9d74946e652647e9b6c9d59326aef104'

    SDBM.open 'local_storage' do |db|
      @access_token = db["access_token"]
      @refresh_token = db["refresh_token"]
    end
  end

  def login
    ap 'Login In...'

    data = default_data.merge({
      user_name: @username,
      password: Digest::MD5.hexdigest(Digest::MD5.hexdigest(@password))
    })

    req = self.class.post("/app/user/login", body: data)
    response = req.parsed_response

    raise ServerResponseError, response["msg"] if response["data"].empty?

    store_tokens(response)
  end

  def refresh_token
    puts 'Refreshing token...'

    data = {
      refresh_token: @refresh_token,
    }

    response = self.class.post("/app/user/refresh_token", body: data).parsed_response

    store_tokens(response)
  end

  def list_devices
    ap "Doing get_object_list..."
    if (!@access_token)
      login
    end

    response = self.class.post("/app/v2/home_page/get_object_list", body: default_data).parsed_response

    if (response["msg"] === 'AccessTokenError')
      refresh_token
      list_devices
    end
    ap response["data"]
  end

  private

  def store_tokens(response)
    @access_token = response["data"]["access_token"]
    @refresh_token = response["data"]["refresh_token"]

    SDBM.open 'local_storage' do |db|
      db["access_token"] = @access_token
      db["refresh_token"] = @refresh_token
    end
  end

  def default_data
    {
      access_token: @access_token,
      phone_id: @phone_id,
      app_ver: @app_ver,
      sc: @sc,
      sv: @sv,
      ts: Time.now.to_i
    }
  end
#
#   def runAction(instanceId, providerKey, actionKey)
#     result
#     begin do
#       getTokens
#       if (!@accessToken)
#         login
#       end
#
#       const data = {
#         provider_key: providerKey,
#         instance_id: instanceId,
#         action_key: actionKey,
#         action_params: {},
#         custom_string: '',
#       }
#
#       result = await axios.post(`${@baseUrl}/app/v2/auto/run_action`, await @getRequestBodyData(data))
#
#       if (result.data.msg === 'AccessTokenError') {
#         await @getRefreshToken
#         return @runAction(instanceId, actionKey)
#       }
#     rescue StandardError => e
#       puts 'Error...', e)
#       throw e
#     end
#     return result.data
#   end
#
#   def getDeviceInfo(deviceMac, deviceModel)
#     result
#     begin do
#       await @getTokens;
#       if (!@accessToken) {
#         await @login
#       }
#       const data = {
#         device_mac: deviceMac,
#         device_model: deviceModel,
#       }
#       result = await axios.post(`${@baseUrl}/app/v2/device/get_device_info`, await @getRequestBodyData(data))
#     rescue StandardError => e
#       puts 'Error...', e)
#       throw e
#     end
#     return result.data.data
#   end
#
#
#   def getPropertyList(deviceMac, deviceModel)
#     result
#     begin do
#       await @getTokens;
#       if (!@accessToken) {
#         await @login
#       }
#       const data = {
#         device_mac: deviceMac,
#         device_model: deviceModel,
#       }
#       result = await axios.post(`${@baseUrl}/app/v2/device/get_property_list`, await @getRequestBodyData(data))
#     rescue StandardError => e
#       puts 'Error...', e)
#       throw e
#     end
#     return result.data.data.property_list
#   end
#
#   def setProperty(deviceMac, deviceModel, propertyId, propertyValue)
#     result
#     begin do
#       await @getTokens;
#       if (!@accessToken) {
#         await @login
#       }
#       const data = {
#         device_mac: deviceMac,
#         device_model: deviceModel,
#         pid: propertyId,
#         pvalue: propertyValue,
#       }
#       const result = await axios.post(`${@baseUrl}/app/v2/device/set_property`, await @getRequestBodyData(data))
#
#     rescue StandardError => e
#       puts 'Error...', e)
#       throw e
#     end
#     return result.data
#   end

#
#   def getDeviceList
#     const result = await @getObjectList
#     return result.data.device_list
#   end
#
#   def getDeviceByName(nickname)
#     const result = await @getDeviceList
#     const device = result.find(device => device.nickname.toLowerCase === nickname.toLowerCase)
#     return device
#   end
#
#   def getDeviceByMac(mac)    const result = await @getDeviceList
#     const device = result.find(device => device.mac === mac)
#     return device
#   end
#
#   def getDevicesByType(type)    const result = await @getDeviceList
#     const devices = result.filter(device => device.product_type.toLowerCase === type.toLowerCase)
#     return devices
#   end
#
#   def getDevicesByModel(model)    const result = await @getDeviceList
#     const devices = result.filter(device => device.product_model.toLowerCase === model.toLowerCase)
#     return devices
#   end
#
#   def getDeviceGroupsList    const result = await @getObjectList
#     return result.data.device_group_list
#   end
#
#   def getDeviceSortList {
#     const result = await @getObjectList()
#     return result.data.device_sort_list
#   end
#
#   def turnOn(device)
#     return await @runAction(device.mac, device.product_model, 'power_on')
#   end
#
#   def turnOff(device)
#     return await @runAction(device.mac, device.product_model, 'power_off')
#   end
#
#   def getDeviceStatus(device)
#     return device.device_params
#   end
#
#   def getDeviceState(device)
#     state = device.device_params.power_switch !== undefined ? (device.device_params.power_switch === 1 ? 'on' : 'off') : ''
#     if (!state)
#       state = device.device_params.open_close_state !== undefined ? (device.device_params.open_close_state === 1 ? 'open' : 'closed') : ''
#     end
#
#     state
#   end

end

w = Wyze.new
w.list_devices
